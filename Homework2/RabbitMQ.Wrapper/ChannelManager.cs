﻿using RabbitMQ.Client;

namespace RabbitMQ.Wrapper
{
    public class ChannelManager
    {
        public IModel Channel { get; }
        public string ExchangeName { get; }
        public string RoutingKey { get; }
        public string QueueName { get; }

        public ChannelManager(IConnection connection, string exchangeName, string routingKey, string queueName = null)
        {
            ExchangeName = exchangeName;
            RoutingKey = routingKey;
            QueueName = queueName;

            Channel = connection.CreateModel();
            Channel.ExchangeDeclare(exchangeName, ExchangeType.Direct);
            if (queueName is not null)
            {
                Channel.QueueDeclare(queue: queueName,
                               durable: true,
                               exclusive: false,
                               autoDelete: false);

                Channel.QueueBind(queueName, exchangeName, routingKey);
            }

        }
    }
}
