﻿using RabbitMQ.Client;
using System;
using System.Text;
using RabbitMQ.Wrapper;
using System.Threading;

namespace Ponger
{
    class Program
    {
        static void Main(string[] args)
        {

            ConnectionFactoryManager factory = new(new Uri(PongerSettings.Uri));
            ChannelManager channel = new(factory.Connection, PongerSettings.ExhangeName, PongerSettings.ProducerRoutingKey);
            ProducerManager producer = new(channel);
          
            ChannelManager channelConsumer = new(factory.Connection, PongerSettings.ExhangeName, PongerSettings.ConsumerRoutingKey,PongerSettings.QueueName);
            ConsumerManager consumer = new(channelConsumer);
            consumer.ListenToQueue((model, ea) =>
            {
                var body = ea.Body.ToArray();
                var message = Encoding.UTF8.GetString(body);
                Console.WriteLine("[{0}] Received {1}", DateTime.Now, message);
                channelConsumer.Channel.BasicAck(ea.DeliveryTag, false);
           
                Thread.Sleep(PongerSettings.TimeDelay);
                producer.Send("Pong");
            });
           
            Console.WriteLine(" Press [enter] to exit.");
            Console.ReadLine();
        }
    }
}
