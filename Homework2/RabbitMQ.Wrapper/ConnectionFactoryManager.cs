﻿using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RabbitMQ.Wrapper
{
    public class ConnectionFactoryManager
    {
        private readonly ConnectionFactory factory;
        public IConnection Connection { get; set; }
        public ConnectionFactoryManager(Uri uri)
        {
            factory = new ConnectionFactory() { Uri = uri };
            Connection = factory.CreateConnection();
        }
    }
}
