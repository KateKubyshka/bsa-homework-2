﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pinger
{
    public static class PingerSettings
    {
        public static string Uri { get; } = "amqp://guest:guest@localhost:5672";
        public static string ExhangeName { get; } = "BestExchange";
        public static string ConsumerRoutingKey { get; } = "ping";
        public static string ProducerRoutingKey { get; } = "pong";
        public static string QueueName { get; } = "ping_queue";
        public static int TimeDelay { get; } = 2500;
    }
}
