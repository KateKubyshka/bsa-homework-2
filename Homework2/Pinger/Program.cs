﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Wrapper;
using System;
using System.Text;
using System.Threading;

namespace Pinger
{
    class Program
    {
        static void Main(string[] args)
        {
            
            ConnectionFactoryManager factory = new(new Uri(PingerSettings.Uri));
            ChannelManager channel = new(factory.Connection, PingerSettings.ExhangeName, PingerSettings.ConsumerRoutingKey, PingerSettings.QueueName);
            ConsumerManager consumer = new(channel);
            ChannelManager channelProducer = new(factory.Connection,PingerSettings.ExhangeName, PingerSettings.ProducerRoutingKey);
            ProducerManager producer = new(channelProducer);
            producer.Send("Ping");
            consumer.ListenToQueue((model, ea) =>
            {
                var body = ea.Body.ToArray();
                var message = Encoding.UTF8.GetString(body);
                Console.WriteLine("[{0}] Received {1}",  DateTime.Now, message);
                channel.Channel.BasicAck(ea.DeliveryTag, false);
                Thread.Sleep(PingerSettings.TimeDelay);
                producer.Send("Ping");
            });

            Console.WriteLine(" Press [enter] to exit.");
            Console.ReadLine();
        }
    }
}
