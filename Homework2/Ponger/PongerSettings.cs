﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ponger
{
    public static class PongerSettings
    {
        public static string Uri { get; } = "amqp://guest:guest@localhost:5672";
        public static string ExhangeName { get; } = "BestExchange";
        public static string ConsumerRoutingKey { get; } = "pong";
        public static string ProducerRoutingKey { get; } = "ping";
        public static string QueueName { get; } = "pong_queue";
        public static int TimeDelay { get; } = 2500;
    }
}
