﻿using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RabbitMQ.Wrapper
{
    public class ProducerManager
    {
        private ChannelManager ChannelModel { get; set; }
      
        public ProducerManager(ChannelManager channel)
        {
            ChannelModel = channel;
 
        }

        public void Send(string message)
        {
            var body = Encoding.UTF8.GetBytes(message);
            ChannelModel.Channel.BasicPublish(exchange:ChannelModel.ExchangeName,
                                routingKey: ChannelModel.RoutingKey,
                                basicProperties: null,
                                body: body);
        }
    }
}
