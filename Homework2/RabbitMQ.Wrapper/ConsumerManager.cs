﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Text;

namespace RabbitMQ.Wrapper
{
    public class ConsumerManager
    {
        private ChannelManager ChannelModel { get; set; }
        private EventingBasicConsumer Consumer { get; }

        public ConsumerManager(ChannelManager channel)
        {
            ChannelModel = channel;
            Consumer = new EventingBasicConsumer(ChannelModel.Channel);
        }

        public void ListenToQueue(EventHandler<BasicDeliverEventArgs> callback)
        {
            Consumer.Received += callback;

            ChannelModel.Channel.BasicConsume(ChannelModel.QueueName, false, Consumer);
        }

    }

}